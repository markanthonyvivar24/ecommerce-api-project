const express = require("express")
const router = express.Router()


const { createProduct, allActiveProducts, getSingleProduct,
updateProduct, archiveProduct, updateProductToActive,  } = require("../controllers/productControllers")
const { verify, verifyAdmin } = require("../auth")

router.get("/activeProducts", allActiveProducts)
router.get("/singleProduct/:id", getSingleProduct)

router.post("/", verify, verifyAdmin, createProduct)


router.put("/updateProduct/:id", verify, verifyAdmin, updateProduct)
router.put("/archiveProduct/:id", verify, verifyAdmin, archiveProduct)
router.put("/makeProductActive/:id",verify, verifyAdmin, updateProductToActive)


module.exports = router