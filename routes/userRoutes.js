const express = require("express")
const router = express.Router()


const { register, login, makeUserAsAdmin, getAllUsers,
createOrder, usersOrders, allOrders, updateUserDetails } = require("../controllers/userControllers")
const { verify, verifyAdmin } = require("../auth")

router.get("/", getAllUsers)
router.get("/usersOrders", verify, usersOrders)
router.get("/allOrders", verify, verifyAdmin, allOrders)


router.post("/createOrder", verify, createOrder)
router.post("/", register) 
router.post("/login", login)



router.put("/updateUserDetails", verify, updateUserDetails)
router.put("/updateUser/:id", verify, verifyAdmin, makeUserAsAdmin)



module.exports = router 
