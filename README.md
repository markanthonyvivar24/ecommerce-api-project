<h2>*** THIS E-COMMERCE API PROJECT WAS DEPLOYED TO HEROKU ***</h2>

<br>

<p>If you want to access this project or try it for some purposes, you may follow these instructions below.</p>

<h5>INSTRUCTIONS:</h5>
<p>Open your postman and go to your workspace. After that, just copy all the links that I've already given below and paste it to your postman so that you can see all the functionalities and features of this Ecommerce API Project. </p>

<p><strong>To get the token of an Admin User, just login this account.</strong>
<div>
{ <br>
    "email": "mark@gmail.com", <br>
    "password": "12345678" <br>
} <br>
</div>
</p>

<h5>POST METHOD FEATURES</h5>
<ul>

<li>Register = https://shrouded-hollows-88195.herokuapp.com/api/users
<p>To register your account, you may use the format fields below to register your account.
<div>
{ <br>
    "firstName": "Jommel", <br>
    "lastName": "Vivar",  <br>
    "mobileNo": "09212167236",  <br>
    "email": "jom@gmail.com",  <br>
    "password": "12345678",  <br>
    "confirmPassword": "12345678"
   
} <br>
</div>
</p>
</li>
<li>Login (To get the token) = https://shrouded-hollows-88195.herokuapp.com/api/users/login
<p>To get the token, make sure that you are already registered before login. Here is the format to login your registered account.
<div>
{ <br>
    "email": "lorie@gmail.com", <br>
    "password": "12345678" <br>
} <br>
</div>
</p>
</li> <br>
<li>Create Product (Admin) = https://shrouded-hollows-88195.herokuapp.com/api/products
<p>For create product feature, you have to make sure that the token that you are using is admin. For your input, These are the fields that you may use to create your own product.</p>
<div>
{ <br>
    "name": "Iphone 10", <br>
    "description": "Description of Iphone 10", <br>
    "price": 30000 <br>
}
</div><br>
</li>

<li>Create Order (Registered User) = https://shrouded-hollows-88195.herokuapp.com/api/users/createOrder
<p>For create order feature, it is possible to create multiple orders. As you can see the fields below, you need to get the Ids of product that you want to buy from the <strong>SHOW ACTIVE PRODUCTS (Get Method)</strong> to put it into the productIds array. And for the total amount field, you have to add those product prices that you want to buy on your own (For the meantime) since I'm planning to implement the adding product prices on frontend.</p>
<div>
{ <br>
    
    "productIds": ["60becba4d1dd3912b4aaf851", "60bed095d1dd3912b4aaf856"],  
    "totalAmount": 78000 
}
</div> 
</li>

</ul>

<h5>PUT METHOD FEATURES</h5>

<ul>
<li>Make User As Admin (Admin) =   https://shrouded-hollows-88195.herokuapp.com/api/users/updateUser
<p>For make user as admin feature, you have to make sure that the logged in user or token that you are using is admin. As you can see the format below, you need to get the user id or the <strong>_id</strong> field from the user details <strong>(go to SHOW ALL USERS method)</strong> to get one user id and put it to url to make this user as admin. </p>
<div><strong>URL:</strong>  https://shrouded-hollows-88195.herokuapp.com/api/users/updateUser/ ( Put here the user id that you want to change from non admin into admin user )</div>
</li><br>

<li>Archive Product (Admin) =   https://shrouded-hollows-88195.herokuapp.com/api/products/archiveProduct
<p>For archive product feature, you have to make sure that the logged in user or token that you are using is admin. Same with <strong>Make User As Admin</strong> method, you need to get the <strong>_id</strong> from the product details <strong>(go to SHOW ACTIVE PRODUCTS method)</strong> that you want to archive and put it to your url.</p>
<div><strong>URL:</strong>  https://shrouded-hollows-88195.herokuapp.com/api/products/archiveProduct/ ( Put here the product id that you want to archive )</div>
</li><br>

<li>Update User Details (Registered User) = https://shrouded-hollows-88195.herokuapp.com/api/users/updateUserDetails
<p>For update user details feature, You have to make sure that you are already logged in. These are the fields that you may change for your account details.</p>
<div>
{<br>
    "firstName": "Manuel", <br>
    "lastName": "Vivar", <br>
    "mobileNo": "09212167267", <br>
    "email": "manuel@gmail.com" <br>
}<br>
</div>
</li><br>

<li>Update Product Details (Admin) =   https://shrouded-hollows-88195.herokuapp.com/api/products/updateProduct
<p>For update product details feature, you have to make sure that the logged in user or token that you are using is admin. As you can see the format below, you need to get the product id or the <strong>_id</strong> field from the product details <strong>(go to SHOW ACTIVE PRODUCTS method)</strong> to get one product id and put it to url to make some changes to your product details.
<div>
{ <br>
    "name": "Sperry Top-Sider Anglefish Shoes", <br>
    "description": "Genuine hand-sewn construction for durable comfort", <br>
    "price": 5000 <br>
} <br>
</div>
</p>
</li>
</ul>

<h5>GET METHOD FEATURES</h5>
<ul>
<li>Show All Users (Non-Registered or Registered User) = https://shrouded-hollows-88195.herokuapp.com/api/users</li>
<li>Show Active Products (Non-Registered or Registered User) = https://shrouded-hollows-88195.herokuapp.com/api/products/activeProducts</li>
<li>Show Single Product (Non-Registered or Registered User) = https://shrouded-hollows-88195.herokuapp.com/api/products/singleProduct
<p>For show single product feature, to show one active product, get the <strong>_id</strong> from the product details <strong>(go to SHOW ACTIVE PRODUCTS method)</strong> and paste it to your url.
<div>
<strong>URL:</strong> https://shrouded-hollows-88195.herokuapp.com/api/products/singleProduct/ ( Put here the product id that you want to show )
</div>
</p>
</li> <br>
<li>Show User's Orders (Registered User) = https://shrouded-hollows-88195.herokuapp.com/api/users/usersOrders</li>
<li>Show All Users Order (Admin) = https://shrouded-hollows-88195.herokuapp.com/api/users/allOrders</li>
</ul>
<br>



<h2>*** THIS ECOMMERCE API IS STILL ON PROCESS FOR ADDING SOME FEATURES ***</h2>









