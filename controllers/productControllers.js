const Product = require("../models/Product")
const Order = require("../models/Order")
 

module.exports.createProduct = (req, res) => {


    let product = new Product({

        name: req.body.name,
        description: req.body.description,
        price: req.body.price

    })

    product.save().then(product => {
        res.send(product)
    }).catch(err => {
        res.send(err)
    })


}


module.exports.allActiveProducts = (req, res) => {

    Product.find({ isActive: true } , {customers: 0}).then(products => {
        res.send(products)
    }).catch(err => {
        res.send(err)
    })


}

module.exports.getSingleProduct = (req, res) => {

    Product.findById(req.params.id, {customers: 0})
        .then(product => {
        res.send(product)
        }).catch(err => {
        res.send(err)
    })
}

module.exports.updateProduct = (req, res) => {

    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
     }
 
     Product.findByIdAndUpdate(req.params.id, updatedProduct, { new: true })
         .then(updatedProduct => {
         res.send(updatedProduct)
         }).catch(err => {
         res.send(err)
     })
}

module.exports.archiveProduct = (req, res) => {

    let updatedProduct = {
       isActive: false
    }
 
     Product.findByIdAndUpdate(req.params.id, updatedProduct, { new: true })
         .then(updatedProduct => {
         res.send(updatedProduct)
         }).catch(err => {
         res.send(err)
     })
}

module.exports.updateProductToActive = (req, res) => {

    let updatedProduct = {
       isActive: true
    }
 
     Product.findByIdAndUpdate(req.params.id, updatedProduct, { new: true })
         .then(updatedProduct => {
         res.send(updatedProduct)
         }).catch(err => {
         res.send(err) 
     })
}


