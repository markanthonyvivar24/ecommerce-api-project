const User = require("../models/User")
const Order = require("../models/Order")
const Product = require('../models/Product')
const bcrypt = require("bcrypt")

const { createAccessToken } = require("../auth.js")



module.exports.register = (req, res) => { 

    const hashedPassword = bcrypt.hashSync(req.body.password, 10);

    

    if (req.body.password.length < 8) return res.send(`Make sure the password characters are greater or equal to 8`)
    if (req.body.password !== req.body.confirmPassword) return res.send(`Make sure the password and confirm password are the same.`)
    

    User.find({ email: req.body.email }).then(user => {
        if (user.length > 0) {
            return res.send("Email Exists!")
        } else {
            let user = new User({

                firstName: req.body.firstName,
                lastName: req.body.lastName,
                mobileNo: req.body.mobileNo,
                email: req.body.email,
                password: hashedPassword,
                
            
                })
            
            
                user.save().then(user => {
                    res.send(user)
                }).catch(err => {
                    res.send(err)
                })
        }
    })
  
    
    

  



}

module.exports.login = (req, res) => {
    
    User.findOne({ email: req.body.email }).then(foundUser => {
        if (foundUser === null) {
            res.send(false);
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
            if (isPasswordCorrect) {
                res.send({ accessToken: createAccessToken(foundUser) });
            } else {
                res.send(false); 
            }

       }
    }).catch(err => {
       res.send(err)
   })

}

module.exports.makeUserAsAdmin = (req, res) => {
    
    let updatedUser = { 
       isAdmin: true
    }

    User.findByIdAndUpdate(req.params.id, updatedUser, { new: true })
        .then(updatedUser => {
        res.send(updatedUser)
        }).catch(err => {
        res.send(err) 
    })
}

module.exports.getAllUsers = (req, res) => {
    User.find({},{orders: 0}).then(users => {
        res.send(users)
    }).catch(err => {
        res.send(err) 
    })
}





module.exports.createOrder = (req, res) => {
   
    if (req.user.isAdmin === true) return res.send({ auth: "failed" })
    
    User.findById(req.user.id).then(foundUser => {
        foundUser.orders.push(req.body)
        return foundUser.save()
    }).then(user => { 
        // console.log(user)
        return Product.find({ _id: req.body.productIds })
    }).then(product => {
        // console.log(product)
      
        product.forEach(product => {
            
            // console.log(product)
            product.customers.push({ userId: req.user.id })
         
            return product.save()
        })
     
    }).then(product => {
        res.send("Successfully ordered!")
    }).catch(err => {
        res.send(err)
    })
}


module.exports.usersOrders = (req, res) => {


    User.findById(req.user.id).then(userFound => {
        if (userFound.orders.length > 0) {
            res.send(userFound)
        } else {
            res.send("Your account has no order!")
        }
    }).catch(err => {
        res.send(err)
    })


}



module.exports.allOrders = (req, res) => {


    User.find().then(user => {
        let usersOrders = user.filter(user => user.orders.length > 0)
        res.send(usersOrders)
    }).catch(err => {
        res.send(err)
    })


}



module.exports.updateUserDetails = (req, res) => {


    let updatedUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
        email: req.body.email,
    }


    User.findByIdAndUpdate(req.user.id, updatedUser, { new: true })
        .then(newUserDetails => {
        res.send(newUserDetails)
        }).catch(err => {
        res.send(err)
    })


}













