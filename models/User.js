const mongoose = require("mongoose")


const UserSchema = new mongoose.Schema({ 

    firstName: {
        type: String,
        required: [true, "First name is required!"]
    },

    lastName: {
        type: String,
        required: [true, "Last name is required!"]
    },

    mobileNo: {
        type: String,
        required: [true, "Mobile no. is required!"]
    },

    email: {
        type: String,
        required: [true, "Email is required!"]
    },

    password: {
        type: String,
        required: [true, "Password is required!"]
    },

    confirmPassword: {
        type: String
    },

    isAdmin: {
        type: Boolean,
        default: false
    },

    orders: [   
        {
            productIds: [
                {
                type: String,
                required: [true, "Product Id is required!"]
                }
            ],

            totalAmount: {
                type: Number,
                required: [true, "Total amount Id is required!"]
            },

            purchasedOn: {
                type: Date,
                default: new Date()
            },
        }
    ]
})


module.exports = mongoose.model("User", UserSchema)