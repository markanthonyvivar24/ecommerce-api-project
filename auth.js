const jwt = require("jsonwebtoken")
const secret = "OnlineStoreFrontApi"


module.exports.createAccessToken = user => { 

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
   
    return jwt.sign(data, secret, {}) 

}

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization
    if (token === "undefined") {
        res.send({auth: "failed"})
    } else {
        token = token.slice(7, token.length);

        jwt.verify(token, secret, function (err, decoded) { 
            if (err) {
                res.send({auth: "failed"})
            } else {
                req.user = decoded
                next();  
            }
        })
    }
}

module.exports.verifyAdmin = (req, res, next) => {

    if (req.user.isAdmin) {
        next()
    } else {
        res.send({auth: "failed"})
    }

}