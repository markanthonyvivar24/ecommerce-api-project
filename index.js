const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const app = express()
const port = process.env.PORT || 5000


mongoose.connect(`mongodb+srv://markvivar123:L3g3nd4ry@cluster0.rkwd4.mongodb.net/
ecommerce-online_store_front-api?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log("Connected to database") 
}).catch(err => {
    console.log(err)
})

app.use(cors())
app.use(express.json()); 

const userRoutes = require("./routes/userRoutes")
app.use("/api/users", userRoutes)

const productRoutes = require("./routes/productRoutes")
app.use("/api/products", productRoutes)

// VERSION 2.0

// const orderRoutes = require("./routes/orderRoutes")
// app.use("/api/orders", orderRoutes)


app.listen(port, () => console.log(`Server is running at port ${port}`))


